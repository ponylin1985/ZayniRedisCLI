﻿using StackExchange.Redis;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace RedisCLI
{
    // dotnet tool install --global --add-source ./Source/Application/bin/Debug zayni-redis-cli
    // dotnet nuget push ./Source/Application/bin/Debug/zayni-redis-cli.6.0.1.nupkg --source https://api.nuget.org/v3/index.json --skip-duplicate -k XXX
    // dotnet nuget push ./Source/Application/bin/Debug/zayni-redis-cli.6.0.1.nupkg --source http://zayni-lab.ddns.net:9011/api/v3/index.json --skip-duplicate --api-key XXX
    // dotnet nuget push ./Source/Application/bin/Debug/zayni-redis-cli.6.0.1.nupkg --source https://gitlab.com/api/v4/projects/18396774/packages/nuget/index.json --skip-duplicate XXX
    /// <summary>
    /// </summary>
    class Program
    {
        // dotnet redis-cli pony-lab.ddns.net:6377:db15 --password Admin7852 upload webapi.test:WebAPI.Test.dll.config /Users/pony/GitRepo/MyGitLab/ZayniFramework/Test/WebAPI.Test/Configs/WebAPI.Test.dll.config
        /// <summary>
        /// dotnet redis-cli localhost:6379:db6 --password Admin123 upload kkk /Users/pony/GitRepo/MyGitLab/ZayniFramework/Test/WebAPI.Test/Configs/WebAPI.Test.dll.config
        /// dotnet redis-cli localhost:6379:db6 --password Admin123 upload {key} {fileName}
        /// localhost:6379:db6 --password Admin123 upload {key} {fileName}
        /// localhost:6379:db6 upload {key} {fileName}
        /// </summary>
        /// <param name="args"></param>
        static async Task Main( string[] args )
        {
            if ( args.IsNullOrEmpty() )
            {
                await HelpAsync();
                return;
            }

            if ( args.Contains( "--help" ) && 1 == args.Length )
            {
                await HelpAsync();
                return;
            }

            var connString = "";
            var host       = "localhost";
            var port       = 6379;
            var dbNumber   = 0;
            var password   = "";
            var key        = "";
            var filePath   = "";
            var arguments  = args.Where( g => g.IsNotNullOrEmpty() ).ToArray();

            if ( arguments.Contains( "--password" ) )
            {
                if ( 6 != arguments.Length )
                {
                    await HelpAsync();
                    throw new ArgumentException( $"Invalid CLI arguments." );
                }

                connString = arguments[ 0 ];
                password   = arguments[ 2 ];
                key        = arguments[ 4 ];
                filePath   = arguments[ 5 ];
            }
            else
            {
                if ( 4 != arguments.Length )
                {
                    await HelpAsync();
                    throw new ArgumentException( $"Invalid CLI arguments." );
                }

                connString = arguments[ 0 ];
                key        = arguments[ 2 ];
                filePath   = arguments[ 3 ];
            }

            var tokens = connString.Split( ':' );

            if ( tokens.IsNullOrEmpty() || tokens.Length != 3 )
            {
                await HelpAsync();
                throw new ArgumentException( $"Invalid CLI arguments." );
            }

            var strPort = tokens[ 1 ];
            var strDb   = tokens[ 2 ];

            if ( !strDb.StartsWith( "db" ) )
            {
                await HelpAsync();
                throw new ArgumentException( $"Invalid CLI arguments." );
            }

            strDb = strDb.RemoveFirstAppeared( "db" );

            if ( !int.TryParse( strDb, out dbNumber ) )
            {
                await HelpAsync();
                throw new ArgumentException( $"Invalid CLI arguments. Incorrent redis db number." );
            }

            if ( !int.TryParse( strPort, out port ) )
            {
                throw new ArgumentException( $"Invalid CLI arguments. Incorrent redis service port number." );
            }

            host = tokens[ 0 ];

            var rawData = await File.ReadAllTextAsync( filePath );

            var opt = new ConfigurationOptions()
            {
                EndPoints          = { { host, port } },
                KeepAlive          = 100,
                DefaultDatabase    = dbNumber,
                AllowAdmin         = true,
                ConnectTimeout     = 1000 * 5,
                SyncTimeout        = 1000 * 10,
                AbortOnConnectFail = false
            };

            if ( password.IsNotNullOrEmpty() )
            {
                opt.Password = password;
            }

            var connection = ConnectionMultiplexer.Connect( opt );
            var db = connection.GetDatabase( dbNumber );

            if ( !await db.StringSetAsync( key, rawData ) )
            {
                await HelpAsync();
                throw new ApplicationException( $"An error occured while upload file to remote redis service." );
            }

            await Console.Out.WriteLineAsync( "Uploading file to redis service successfully." );
        }

        /// <summary>顯示 CLI 指令的協助
        /// </summary>
        private static async Task HelpAsync() 
        {
            await Console.Out.WriteLineAsync( "Support commands:" );
            await Console.Out.WriteLineAsync( "dotnet redis-cli {redisHost}:{redisPort}:db{DbNumber} --password {Password} upload {key} {filePath}" );
            await Console.Out.WriteLineAsync( "dotnet redis-cli {redisHost}:{redisPort}:db{DbNumber} upload {key} {filePath}" );
        }
    }
}
